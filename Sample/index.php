<?php
if (PHP_SAPI != 'cli') {
	echo "<pre>";
}

$xCoordinates = array(1,2,3,4,5,6);//non repeating but you can use years or months and must be sorted
$yCoordinates = array(5,2,7,4,3,9);//can be repeating and same count with x coordinates

$yc = array();
$yc = array_fill_keys($xCoordinates,0);
for($i = 0; $i < count($xCoordinates); $i++){
    $yc[$xCoordinates[$i]] = $yCoordinates[$i];
}

require_once __DIR__ . '/../autoload.php';
$regression = new \Reg\Regression();

$compute = $regression->computeRawArray($xCoordinates,$yCoordinates);
$compute2 = $regression->computeWithArrayKeys($xCoordinates,$yc);
$yint = $regression->ysquared($xCoordinates,$compute2);
// foreach($compute as $c){

// }
echo"x values";
print_r($xCoordinates);
echo"y values";
print_r($yCoordinates);
print_r($compute);
print_r($compute2);
print_r($yint);
?>
<html>
<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script href = "https://code.jquery.com/jquery-1.12.4.js"> </script> 
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<title></title>
<body>
<script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Year',"Raw","with Regression"],
          <?php 
          $ctr = 1;
          foreach($xCoordinates as $x) {
            echo "['".$x."', ".$yCoordinates[$ctr-1].",".$yint[$ctr]."],";      
            $ctr+=1;   
          }?>
        ]);

        var options = {
          title: 'Sample Regression graph',
          // curveType: 'function',
          hAxis: {title: 'X Coordinate'},
          vAxis: {title: 'Y Coordinate'},
          legend: { position: 'right' },
        };

        var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));

        chart.draw(data, options);
      }
</script>
    <div id="curve_chart" style="width: 900px; height: 500px"></div>
    <!-- <div><h4> Slope <?=$m?>  Intercept <?=$b?> </h4></div> -->
</body>
</html>