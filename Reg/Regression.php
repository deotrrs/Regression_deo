<?php
namespace Reg;
class Regression {

    function linear_regression($xCoor, $yCoor) {

// calculate number points
        $n = count($xCoor);
        
        // ensure both arrays of points are the same size
        if ($n != count($yCoor)) {

            trigger_error("linear_regression(): Number of elements in coordinate arrays do not match.", E_USER_ERROR);
        
        }           
        $x_sum=0;
        $y_sum = array_sum($yCoor);

        $xx_sum = 0;
        $xy_sum = 0;
        for($i = 0; $i <= $n; $i++) {           
                $x_sum=$x_sum+$i;   // get the total sum of x coordinate not the value of x array    
        }
        $res = array();
        for($j = 0; $j < $n; $j++) {            
            $xy_sum+=($j*$yCoor[$xCoor[$j]]);
            $xx_sum+=($j*$j);
            // array_push($res,$yCoor[$j]);
            // array_push($res,$xy_sum); 
            // array_push($res,$yCoor[$j]);                
        }          
        
            $m = (($n * $xy_sum) - ($x_sum * $y_sum)) / (($n * $xx_sum) - ($x_sum * $x_sum));

        // calculate intercept
        $b = ($y_sum - ($m * $x_sum)) / $n;
        // return result
        return array("m"=>$m, "b"=>$b);

    }

     function computeRawArray($xCoor, $yCoor) {

// calculate number points
        $n = count($xCoor);
        
        // ensure both arrays of points are the same size
        if ($n != count($yCoor)) {

            trigger_error("linear_regression(): Number of elements in coordinate arrays do not match.", E_USER_ERROR);
        
        }           
        $x_sum=0;
        $y_sum = array_sum($yCoor);

        $xx_sum = 0;
        $xy_sum = 0;
        for($i = 0; $i <= $n; $i++) {           
                $x_sum=$x_sum+$i;   // get the total sum of x coordinate not the value of x array    
        }
        $res = array();
        for($j = 0; $j < $n; $j++) {            
            $xy_sum+=($j*$yCoor[$j]);
            $xx_sum+=($j*$j);
            // array_push($res,$yCoor[$j]);
            array_push($res,$xx_sum); 
            // array_push($res,$yCoor[$j]);                
        }          
        
            $m = (($n * $xy_sum) - ($x_sum * $y_sum)) / (($n * $xx_sum) - ($x_sum * $x_sum));

        // calculate intercept
        $b = ($y_sum - ($m * $x_sum)) / $n;
        // return result
        return array("m"=>$m, "b"=>$b, "i"=>$res);

    }

    function computeWithArrayKeys($xCoor, $yCoor) {

// calculate number points
        $n = count($xCoor);
        
        // ensure both arrays of points are the same size
        if ($n != count($yCoor)) {

            trigger_error("linear_regression(): Number of elements in coordinate arrays do not match.", E_USER_ERROR);
        
        }           
        // $x_sum=0;
        $x_sum= array_sum($xCoor);
        $y_sum = array_sum($yCoor);

        $xx_sum = 0;
        $xy_sum = 0;
        $res = array();
        for($j = 0; $j < $n; $j++) {            
            $xy_sum+=($j*$yCoor[$xCoor[$j]]);
            $xx_sum+=($j*$j);
            // array_push($res,$yCoor[$j]);
            array_push($res,$xx_sum); 
            // array_push($res,$yCoor[$j]);                
        }          
        
            $m = (($n * $xy_sum) - ($x_sum * $y_sum)) / (($n * $xx_sum) - ($x_sum * $x_sum));

        // calculate intercept
        $b = ($y_sum - ($m * $x_sum)) / $n;
        // return result
        return array("m"=>$m, "b"=>$b, "i"=>$res ,"sad"=>(($n * $xy_sum) - ($x_sum * $y_sum)) ,"qw"=>(($n * $xx_sum) - ($x_sum * $x_sum)));

    }

    public function computeSlope($rise, $run){
        $slope = $rise/$run;
        return $slope;
    }

    public function getIntercept($b){
        return $b;
    }

    public function ysquared($xCoor,$reg){
        $ysquared = array();
        $ysquaredsum = 0;
        for($i = 0; $i < count($xCoor); $i++){                    
                    $ysquaredsum = $reg["m"] + $reg["b"]*$i;
                    $ysquared[$xCoor[$i]] = $ysquaredsum;                                                 
            }
        return $ysquared;
    }  
} 

?>